package java_basics;

public class Variables {

	public static void main(String[] args) {
		
		int x = 34;
		
		System.out.println(x);
		
		String words = "this is a sentence";
		
		System.out.println(words);
	}

}
