package java_basics;

public class VariableContinued {

	public static void main(String[] args) {
		
		int var = 100000000;
		long bigNumber = 100000000000000L;
		short smallNumber = -32768;
		byte reallySmallNumber = -128;
		double decimalVariable = 394.003;
		boolean decision = false;
		char letter = 'k';
		
		System.out.println(var);
		System.out.println(bigNumber);
		System.out.println(smallNumber);
		System.out.println(reallySmallNumber);
		System.out.println(decimalVariable);
		System.out.println(decision);
		System.out.println(letter);
	}

}
